#include "RGBVal.hpp"

class BaryCentCoords {
public:
    
    BaryCentCoords(): alpha(0), beta(0),gamma(0) {}

    BaryCentCoords(float a, float b, float c): alpha(a), beta(b), gamma(c) {}

    float alpha;
    float beta;
    float gamma;

    bool IsInTriangle();

    RGBVal GetColour(RGBVal rgbAlpha, RGBVal rgbBeta, RGBVal rgbGamma);

};