#ifndef _PIXEL_WIDGET_
#define _PIXEL_WIDGET_

#include <QtGui>
#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QPainter>
#include <QLabel>
#include <QDesktopWidget>
#include <iostream>
#include <vector>
#include "RGBVal.hpp"
#include "baryCentCoords.hpp"


class PixelWidget : public QWidget {
public:

  
  // set the number of pixels that the widget is meant to display
  PixelWidget
  (
   unsigned int n_horizontal, // the first integer determines the number of horizontal pixels 
   unsigned int n_vertical    // the second integer determines the number of vertical pixels
   );

  // sets a pixel at the specified RGBVal value; ignores non-existing pixels without warning
  void SetPixel
  ( 
   unsigned int  i_x, // horizontal pixel coordinate
   unsigned int  i_y, // vertical pixel coordinate
   const RGBVal& c    // RBGVal object for RGB values
    );

  // Use the body of this function to experiment with rendering algorithms
  void DefinePixelValues();

  // Draws a line between 2 given points
  void DrawLine
  (
    float x1, // x value of starting point of the line
    float y1, // y value of starting point of the line
    float x2, // x value of finishing point of the line
    float y2 // y value of finishing point of the line
  );

  void DrawLine
  (
    float x1,
    float y1,
    RGBVal rgb1,
    float x2,
    float y2,
    RGBVal rgb2
  );

  void DrawTriangle
  (
    float x1,
    float y1,
    RGBVal rgb1,
    float x2,
    float y2,
    RGBVal rgb2,
    float x3,
    float y3,
    RGBVal rgb3
  );

  BaryCentCoords GetBaryCentCoords
  (
    float x,
    float y,
    float x1,
    float y1,
    float x2,
    float y2,
    float x3,
    float y3
  );

  void DrawTriangleAndReport
  (
    float x1,
    float y1,
    RGBVal rgb1,
    float x2,
    float y2,
    RGBVal rgb2,
    float x3,
    float y3,
    RGBVal rgb3
  );

  void CreateImageFile();

protected:

  virtual void paintEvent(QPaintEvent*);


private:

  unsigned int _n_vertical;
  unsigned int _n_horizontal;

  std::vector<std::vector<RGBVal> > _vec_rects;

  bool IsPointOnSameSideOfLine(float x, float y, float x1, float y1,float x2, float y2, float x3, float y3 );
  bool IsInside(float x, float y, float x1, float y1,float x2, float y2, float x3, float y3 );
};

#endif

