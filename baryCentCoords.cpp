#include "baryCentCoords.hpp"

bool BaryCentCoords::IsInTriangle()
{
    if (alpha >= 0 && alpha <= 1)
    {
        if (beta >= 0 && beta <= 1)
        {
            if (gamma >= 0 && gamma <= 1)
            {
                return true;
            }
        }
    }
    return false;
}

RGBVal BaryCentCoords::GetColour(RGBVal rgbAlpha, RGBVal rgbBeta, RGBVal rgbGamma)
{
    int red = alpha*rgbAlpha._red + beta*rgbBeta._red + gamma*rgbGamma._red;
    int green = alpha*rgbAlpha._green + beta*rgbBeta._green + gamma*rgbGamma._green;
    int blue = alpha*rgbAlpha._blue + beta*rgbBeta._blue + gamma*rgbGamma._blue;
    return RGBVal(red,green,blue);
}