#include <QtGui>
#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QPainter>
#include <QLabel>
#include <QDesktopWidget>
#include <iostream>
#include <fstream>
#include "pixelwidget.hpp"


void PixelWidget::DefinePixelValues(){ //add pixels here; write methods like this for the assignments
  // Test for assignmanet 1
  // DrawLine(10, 10, 60,30);

  // Test for assignment 2
  // DrawLine(10,10,RGBVal(0,0,255) ,60,20, RGBVal(255,0,0));
  // DrawLine(10, 35, RGBVal(255,255,0), 60, 25, RGBVal(0,0,255));
  // DrawLine(10, 40, RGBVal(255, 0, 255), 60,60, RGBVal(255,255,0));

  // Test for assignment 3
  DrawTriangle(35,10,RGBVal(255,255,0), 10,55,RGBVal(0,255,255), 60, 40, RGBVal(255,0,255) );

  // Test for assignment 4
  // DrawTriangleAndReport(35,10,RGBVal(255,255,0), 10,55,RGBVal(0,255,255), 60, 40, RGBVal(255,0,255) );
  
  // Test for assignment 5
  // DrawTriangle(35,10,RGBVal(255,255,0), 10,55,RGBVal(0,255,255), 60, 40, RGBVal(255,0,255) );
  // CreateImageFile();
}


// -----------------Most code below can remain untouched -------------------------
// ------but look at where DefinePixelValues is called in paintEvent--------------

PixelWidget::PixelWidget(unsigned int n_vertical, unsigned int n_horizontal):
  _n_vertical   (n_vertical),
  _n_horizontal (n_horizontal),
  _vec_rects(0)
{
  // all pixels are initialized to black
     for (unsigned int i_col = 0; i_col < n_vertical; i_col++)
       _vec_rects.push_back(std::vector<RGBVal>(n_horizontal));
}



void PixelWidget::SetPixel(unsigned int i_column, unsigned int i_row, const RGBVal& rgb)
{

  // if the pixel exists, set it
  if (i_column < _n_vertical && i_row < _n_horizontal)
    _vec_rects[i_column][i_row] = rgb;
}

void PixelWidget::DrawLine(float x1, float y1, float x2, float y2)
{
  float dx = x2-x1;
  float dy = y2-y1;
  
  float tStep = 1 / (qMax(dx,dy)+1);

  for (float t = 0; t <= 1; t += tStep){
    SetPixel(round(x1 + (dx  * t)), round(y1 + (dy * t)), RGBVal(255,255,255));
  }

}

void PixelWidget::DrawLine(float x1, float y1, RGBVal rgb1, float x2, float y2, RGBVal rgb2)
{
  float dx = x2-x1;
  float dy = y2-y1;
  int dr = rgb2._red - rgb1._red;
  int dg = rgb2._green - rgb1._green;
  int db = rgb2._blue - rgb1._blue;

  float tStep = 1 / (qMax(dx,dy)+1);

  for (float t = 0; t <= 1; t += tStep)
  {
    int red = rgb1._red + (t*dr);
    int green = rgb1._green + (t*dg);
    int blue = rgb1._blue + (t*db);
      
    SetPixel(round(x1 + (dx  * t)), round(y1 + (dy * t)), RGBVal(red, green, blue));
  }
}

void PixelWidget::DrawTriangle(float x1, float y1, RGBVal rgb1, float x2, float y2, RGBVal rgb2, float x3, float y3, RGBVal rgb3)
{
  float xMin = qMin(x1,qMin(x2,x3));
  float xMax = qMax(x1,qMax(x2,x3));
  float yMin = qMin(y1,qMin(y2,y3));
  float yMax = qMax(y1,qMax(y2,y3));

  for (int x = xMin; x <= xMax; x++)
  {
    for (int y = yMin; y<= yMax; y++)
    {
      BaryCentCoords bcCoords = GetBaryCentCoords(x,y, x1,y1, x2,y2, x3,y3);
      if (bcCoords.IsInTriangle())
      {
        SetPixel(x,y, bcCoords.GetColour(rgb1, rgb2, rgb3));
      }
    }
  }
}

bool PixelWidget::IsPointOnSameSideOfLine(float x, float y, float x1, float y1, float x2, float y2, float x3, float y3)
{
  bool isPositive = ((x3 - x2) * (y1 - y2)) - ((x1 - x2) * (y3 - y2)) > 0;
  if ((((x - x2) * (y1 - y2)) - ((x1 - x2) * (y - y2)) > 0) == isPositive)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool PixelWidget::IsInside(float x, float y, float x1, float y1, float x2, float y2, float x3, float y3)
{
  if (IsPointOnSameSideOfLine(x,y, x1,y1, x2,y2, x3,y3))
  {
    if(IsPointOnSameSideOfLine(x,y, x2,y2, x3,y3, x1,y1))
    {
      if(IsPointOnSameSideOfLine(x,y, x3,y3, x1,y1, x2,y2))
      {
        return true;
      }
    }        
  }
  return false;
}

BaryCentCoords PixelWidget::GetBaryCentCoords(float x, float y, float x1, float y1, float x2, float y2, float x3, float y3)
{
  float alpha = ((y2-y3)*(x-x3) + (x3-x2)*(y-y3)) / ((y2-y3)*(x1-x3) + (x3-x2)*(y1-y3));
  float beta = ((y3-y1)*(x-x3) + (x1-x3)*(y-y3)) / ((y2-y3)*(x1-x3) + (x3-x2)*(y1-y3));
  float gamma = 1 - alpha - beta;
  return BaryCentCoords(alpha, beta, gamma);
}

void PixelWidget::DrawTriangleAndReport( float x1, float y1, RGBVal rgb1, float x2, float y2, RGBVal rgb2, float x3, float y3, RGBVal rgb3)
{
  std::ofstream report;
  report.open("report.txt");
  for(unsigned int x = 0; x < _n_horizontal; x++)
  {
    for (unsigned int y = 0; y < _n_vertical; y++)
    {
      BaryCentCoords bcCoords = GetBaryCentCoords(x,y, x1,y1, x2,y2, x3,y3);
      bool isInside = IsInside(x,y, x1,y1, x2,y2, x3,y3);
      report << "x:" << x << " y:" << y << " alpha:" << bcCoords.alpha << " beta:" << bcCoords.beta << " gamma:" << bcCoords.gamma << " IsInside:" << isInside << "\n";
      if (bcCoords.IsInTriangle())
      {
        SetPixel(x,y, bcCoords.GetColour(rgb1, rgb2, rgb3));
      }
    }
  }
  report.close();
}

void PixelWidget::CreateImageFile()
{
  std::ofstream image;
  image.open("image.ppm");
  image << "P3 " << _n_horizontal << " " << _n_vertical << " 255\n";
  for (int column = 0; column < _n_vertical; column++ )
  {
    for (int row = 0; row < _n_horizontal; row++)
    {
      image << _vec_rects[row][column]._red << " " << _vec_rects[row][column]._green << " " << _vec_rects[row][column]._blue << " ";
    }
    image << "\n";
  }
  image.close();
}

void PixelWidget::paintEvent( QPaintEvent * )
{

  QPainter p( this );
  // no antialiasing, want thin lines between the cell
  p.setRenderHint( QPainter::Antialiasing, false );

  // set window/viewport so that the size fits the screen, within reason
  p.setWindow(QRect(-1.,-1.,_n_vertical+2,_n_horizontal+2));
  int side = qMin(width(), height());  
  p.setViewport(0, 0, side, side);

  // black thin boundary around the cells
  QPen pen( Qt::black );
  pen.setWidth(0.);

  // here the pixel values defined by the user are set in the pixel array
  DefinePixelValues();

  for (unsigned int i_column = 0 ; i_column < _n_vertical; i_column++)
    for(unsigned int i_row = 0; i_row < _n_horizontal; i_row++){
      QRect rect(i_column,i_row,1,1);
      QColor c = QColor(_vec_rects[i_column][i_row]._red, _vec_rects[i_column][i_row]._green, _vec_rects[i_column][i_row]._blue);
    
      // fill with color for the pixel
      p.fillRect(rect, QBrush(c));
      p.setPen(pen);
      p.drawRect(rect);
    }
}
